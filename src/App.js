import "./App.scss";
import Window from "./components/Window/window.component";
import { GameControllerContext } from "./components/Contexts/gameController.component";
import { useContext, useEffect, useState } from "react";
import Form from "./components/Form/form.component";
import { NAMES } from "./components/utils/names.utils";
import Circle from "./components/Circle/circle.component";
import Triangle from "./components/Triangle/triangle.component";
import Square from "./components/Square/square.component";
import DataSet from "./components/DataSet/dataSet.component";
import Button from "./components/Button/button.component";
import ButtonsGroup from "./components/ButtonsGroup/buttonsGroup.component";

const App = () => {
  let movingLoop;
  const {
    isGameStarted,
    objects,
    windowHeight,
    windowWidth,
    setObjects,
    moving,
    setMoving,
    setIsGameStart,
    setShapeType,
  } = useContext(GameControllerContext);

  const moveForward = () => {
    if (moving) {
      movingLoop = setInterval(() => {
        setObjects((prev) => {
          let newArr = [];
          prev.map((eachObj) => {
            let newObj = { ...eachObj, left: eachObj["left"] + 5 };
            newArr.push(newObj);
          });
          return newArr;
        });
      }, 1000);
    } else {
      for (let i = 0; i < 100; i++) {
        clearInterval(i);
      }
    }
  };

  const moveBack = () => {
    if (moving) {
      movingLoop = setInterval(() => {
        setObjects((prev) => {
          let newArr = [];
          prev.map((eachObj) => {
            let newObj = {
              ...eachObj,
              left: eachObj["left"] - 5 > 0 ? eachObj["left"] - 5 : 0,
            };
            newArr.push(newObj);
          });
          return newArr;
        });
      }, 1000);
    } else {
      for (let i = 0; i < 100; i++) {
        clearInterval(i);
      }
    }
  };

  const stop = () => {
    setMoving(true);
    for (let i = 0; i < 100; i++) {
      clearInterval(i);
    }
  };

  const restart = () => {
    setShapeType({
      triangle: true,
      square: true,
      circle: true,
    });
    setIsGameStart(false);
    for (let i = 0; i < 100; i++) {
      clearInterval(i);
    }
    setMoving(true);
  };
  return (
    <div className="App">
      {!isGameStarted ? (
        <Window height={windowHeight} width={windowWidth}>
          <Form />
        </Window>
      ) : (
        <div className="gamePage">
          <div className="upperContainer">
            <Window
              height={windowHeight}
              width={windowWidth / 2}
              margin="0 10px"
              overflowY="auto"
            >
              <DataSet />
            </Window>
            <Window height={windowHeight} width={windowWidth} padding="0px">
              {objects &&
                objects.map(
                  ({
                    width,
                    type,
                    backgroundColor,
                    name,
                    top,
                    left,
                    height,
                    transparent,
                  }) => {
                    switch (type) {
                      case "circle":
                        return (
                          <Circle
                            key={name}
                            backgroundColor={backgroundColor}
                            width={width}
                            top={top}
                            left={left}
                            name={name}
                            transparent={transparent}
                          />
                        );
                      case "triangle":
                        return (
                          <Triangle
                            key={name}
                            backgroundColor={backgroundColor}
                            width={width}
                            height={height}
                            top={top}
                            left={left}
                            name={name}
                            transparent={transparent}
                          />
                        );
                      case "square":
                        return (
                          <Square
                            key={name}
                            backgroundColor={backgroundColor}
                            width={width}
                            height={height}
                            top={top}
                            left={left}
                            name={name}
                            transparent={transparent}
                          />
                        );
                    }
                  }
                )}
            </Window>
          </div>
          <Window
            height="120px"
            width={windowWidth * 1.5 + 15}
            margin="10px 10px"
          >
            <ButtonsGroup margin="5px 0">
              <Button width="20px" height="30px" transShape="circle">
                Transparent Circle
              </Button>
              <Button width="20px" height="30px" transShape="triangle">
                Transparent Triangle
              </Button>
              <Button width="20px" height="30px" transShape="square">
                Transparent Square
              </Button>
            </ButtonsGroup>
            <ButtonsGroup margin="10px 0">
              <Button
                width="20px"
                height="30px"
                onClick={() => {
                  setMoving(!moving);
                  moveForward();
                }}
              >
                Forward
              </Button>
              <Button
                width="20px"
                height="30px"
                onClick={() => {
                  setMoving(!moving);
                  moveBack();
                }}
              >
                Back
              </Button>
              <Button width="20px" height="30px" onClick={stop}>
                Stop
              </Button>
            </ButtonsGroup>
            <ButtonsGroup margin="10px 0">
              <Button width="20px" height="30px" onClick={restart}>
                Restart
              </Button>
            </ButtonsGroup>
          </Window>
        </div>
      )}
    </div>
  );
};

export default App;
