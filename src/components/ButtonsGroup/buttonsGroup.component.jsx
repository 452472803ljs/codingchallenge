import { useState } from "react";
import "./buttons.sytle.scss";

const ButtonsGroup = ({ children, title, error, margin }) => {
  return (
    <div className="buttonsContainer" style={{ margin }}>
      {title && <div className="groupTitle">{title}</div>}
      <div className="buttonsSelection">{children}</div>
    </div>
  );
};

export default ButtonsGroup;
