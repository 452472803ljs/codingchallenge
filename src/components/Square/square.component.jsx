import "./square.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";
import { useContext } from "react";
const Square = ({
  name,
  left,
  top,
  width,
  height,
  backgroundColor,
  transparent,
}) => {
  const { windowHeight, windowWidth } = useContext(GameControllerContext);
  const justiedLeft = width + left > windowWidth ? windowWidth - width : left;
  const justfiedTop =
    height + top >= windowHeight ? windowHeight - height : top;
  return (
    <div
      className="square"
      style={{
        height,
        width,
        top: justfiedTop,
        left: justiedLeft,
        backgroundColor,
        opacity: transparent ? 0.5 : 1,
      }}
    >
      <span>{name}</span>
    </div>
  );
};

export default Square;
