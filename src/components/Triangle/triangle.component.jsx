import { useContext } from "react";
import "./triangle.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";

const Triangle = ({
  name,
  left,
  top,
  width,
  height,
  backgroundColor,
  transparent,
}) => {
  const { windowHeight, windowWidth } = useContext(GameControllerContext);
  const justiedLeft = width + left > windowWidth ? windowWidth - width : left;
  const justfiedTop =
    height + top >= windowHeight ? windowHeight - height : top;

  return (
    <div
      className="triangle"
      style={{
        borderBottom: `${height}px solid ${backgroundColor}`,
        top: justfiedTop,
        left: justiedLeft,
        borderLeft: `${width / 2}px solid transparent`,
        borderRight: `${width / 2}px solid transparent`,
        opacity: transparent ? 0.5 : 1,
      }}
    >
      <span>{name}</span>
    </div>
  );
};

export default Triangle;
