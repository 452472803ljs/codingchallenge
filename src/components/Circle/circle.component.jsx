import { useContext } from "react";
import "./circle.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";

const Circle = ({
  name,
  left,
  top,
  width,
  height,
  backgroundColor,
  transparent,
}) => {
  const { windowHeight, windowWidth } = useContext(GameControllerContext);
  const justiedLeft = width + left > windowWidth ? windowWidth - width : left;
  const justfiedTop =
    height + top >= windowHeight ? windowHeight - height : top;
  return (
    <div
      className="circle"
      style={{
        width,
        height: width,
        backgroundColor,
        borderRadius: "50%",
        top: justfiedTop,
        left: justiedLeft,
        opacity: transparent ? 0.5 : 1,
      }}
    >
      <span>{name}</span>
    </div>
  );
};

export default Circle;
