import "./input.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";
import { useContext, useEffect, useState } from "react";
const Input = ({ labelName, type, id }) => {
  const { setObjectNumber, hint } = useContext(GameControllerContext);

  const [error, setHasError] = useState(false);
  const [number, setNumber] = useState(1);
  const inputHandler = (event) => {
    setNumber(event.target.value);

    if (
      event.target.value === null ||
      event.target.value <= 0 ||
      event.target.value > 50
    ) {
      setHasError(true);
      setObjectNumber(-1);
    } else {
      setObjectNumber(event.target.value);
      setHasError(false);
    }
  };
  return (
    <div className="inputContainer">
      <label htmlFor={id}>{labelName}</label>
      <input
        id={id}
        type={type}
        value={number}
        max={50}
        min={1}
        onChange={inputHandler}
      />
      {error && (
        <p className="hint" style={{ color: error ? "red" : "green" }}>
          {hint}
        </p>
      )}
    </div>
  );
};

export default Input;
