import "./button.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";
import { useContext, useEffect } from "react";
const Button = ({ type, children, height, width, onClick, transShape }) => {
  const {
    shapeType,
    setShapeType,
    setWindowHeight,
    setWindowWidth,
    windowWidth,
    objects,
    setObjects,
  } = useContext(GameControllerContext);
  const shape = children.toString().toLowerCase();
  const typeHandler = () => {
    switch (shape) {
      case "triangle":
        setShapeType({ ...shapeType, triangle: !shapeType["triangle"] });
        break;
      case "square":
        setShapeType({ ...shapeType, square: !shapeType["square"] });
        break;
      case "circle":
        setShapeType({ ...shapeType, circle: !shapeType["circle"] });
        break;
      case "600 * 600":
        setWindowWidth(600);
        setWindowHeight(600);
        break;
      case "800 * 800":
        setWindowWidth(800);
        setWindowHeight(800);
        break;
    }
  };

  const shapeChangleHandler = () => {
    let arr = [];
    objects.map((eachObj) => {
      eachObj.type === transShape
        ? arr.push({ ...eachObj, transparent: !eachObj["transparent"] })
        : arr.push(eachObj);
    });
    setObjects(arr);
  };

  return (
    <button
      className="buttons"
      type={type ? type : "button"}
      style={{
        height,
        width,
        backgroundColor: shapeType[shape] === true && "orangered",
        color: shapeType[shape] === true && "white",
      }}
      onClick={
        onClick ? onClick : transShape ? shapeChangleHandler : typeHandler
      }
    >
      {children}
    </button>
  );
};
export default Button;
