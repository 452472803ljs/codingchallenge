import { useContext } from "react";
import "./dataSet.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";
const DataSet = () => {
  const { objectNumber, objects } = useContext(GameControllerContext);
  return (
    <div className="dataSet">
      <h3>Object Created:</h3>
      <h5>{objectNumber} objects</h5>
      {objects.map(
        ({ width, type, backgroundColor, name, top, left, height }) => {
          return (
            <div className="itemInfo" key={name}>
              <span>type: {type}</span>
              <span>name: {name}</span>
              <span>width: {width}</span>
              <span>height: {height}</span>
              <span>top: {top}</span>
              <span>left: {left}</span>
              <span style={{ flexBasis: "100%" }}>
                backgroundColor: {backgroundColor}
              </span>
            </div>
          );
        }
      )}
    </div>
  );
};
export default DataSet;
