import { useContext, useEffect } from "react";
import Button from "../Button/button.component";
import ButtonsGroup from "../ButtonsGroup/buttonsGroup.component";
import Input from "../Input/input.component";
import "./form.style.scss";
import { GameControllerContext } from "../Contexts/gameController.component";
import { NAMES } from "../utils/names.utils";
const Form = () => {
  const {
    objectNumber,
    shapeType,
    setShapeType,
    isGameStarted,
    setIsGameStart,
    setObjects,
    windowHeight,
    windowWidth,
  } = useContext(GameControllerContext);

  const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min);
  };

  const getRandomColor = () => {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  const generateObjs = () => {
    const emptyArray = [];
    const shapes = [];
    for (const [key, value] of Object.entries(shapeType)) {
      value && shapes.push(key);
    }
    for (let i = 0; i < objectNumber; i++) {
      const obj = {
        name: NAMES[i],
        width: getRandomInt(5, 15),
        height: getRandomInt(5, 15),
        backgroundColor: getRandomColor(),
        left: getRandomInt(0, windowWidth),
        top: getRandomInt(0, windowWidth),
        type: shapes[getRandomInt(0, shapes.length)],
        transparent: false,
      };
      emptyArray.push(obj);
    }
    console.log(emptyArray);
    setObjects(emptyArray);
  };

  const playHandler = () => {
    const hasType = Object.values(shapeType).indexOf(true) > -1;
    objectNumber !== -1 && hasType && setIsGameStart(true);
    generateObjs();
  };
  return (
    <div className="formContainer">
      <Input labelName="How many objects you want to create?" type="number" />
      <ButtonsGroup title="Choose the type you want">
        <Button>Triangle</Button>
        <Button>Square</Button>
        <Button>Circle</Button>
      </ButtonsGroup>
      <ButtonsGroup title="Choose the window size you want">
        <Button width={250}>600 * 600</Button>
        <Button width={250}>800 * 800</Button>
      </ButtonsGroup>
      <Button width="200px" onClick={playHandler}>
        Let's Play
      </Button>
    </div>
  );
};

export default Form;
