import "./window.style.scss";

const Window = ({
  height,
  width,
  backgroundColor,
  children,
  padding,
  margin,
  overflowY,
}) => {
  return (
    <div
      style={{ height, width, backgroundColor, padding, margin, overflowY }}
      className="container"
    >
      {children}
    </div>
  );
};

export default Window;
