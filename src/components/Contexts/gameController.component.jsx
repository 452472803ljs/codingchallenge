import { createContext, useState } from "react";

export const GameControllerContext = createContext({
  isGameStarted: false,
  setIsGameStart: () => {},
  objectNumber: null,
  setObjectNumber: () => {},
  shapeType: {},
  setShapeType: () => {},
  hint: "",
  objects: [],
  setObjects: () => {},
  windowWidth: 600,
  setWindowWidth: () => {},
  windowHeight: 600,
  setWindowHeight: () => {},
  moving: true,
  setMoving: () => {},
});

export const GameControllerProvider = ({ children }) => {
  const hint = "The number of objects can not be empty,zero nor more than 50";
  const [isGameStarted, setIsGameStart] = useState(false);
  const [objectNumber, setObjectNumber] = useState(1);
  const [objects, setObjects] = useState(null);
  const [windowWidth, setWindowWidth] = useState(600);
  const [windowHeight, setWindowHeight] = useState(600);
  const [moving, setMoving] = useState(true);
  const [shapeType, setShapeType] = useState({
    triangle: true,
    square: true,
    circle: true,
  });

  const [hasError, setHasError] = useState(null);
  const value = {
    isGameStarted,
    setIsGameStart,
    objectNumber,
    setObjectNumber,
    shapeType,
    setShapeType,
    hasError,
    setHasError,
    hint,
    objects,
    setObjects,
    windowHeight,
    setWindowHeight,
    windowWidth,
    setWindowWidth,
    moving,
    setMoving,
  };

  return (
    <GameControllerContext.Provider value={value}>
      {children}
    </GameControllerContext.Provider>
  );
};
