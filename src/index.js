import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import App from "./App";
import { GameControllerProvider } from "./components/Contexts/gameController.component";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <GameControllerProvider>
      <App />
    </GameControllerProvider>
  </React.StrictMode>
);
