To get the copy of project

1. git clone https://gitlab.com/452472803ljs/codingchallenge.git
2. cd codingchallenge

To start the program

1. npm i
2. npm start

To play the game

1. input the number of object you want to create (between 1 - 50)
2. select the shape you want to create (at least 1)
3. select the window size (optional)
4. click let's play

Function:

1. transparent shapes or change back to fully visible by clicking transparent button
2. moving shapes to different direction by click direction button
